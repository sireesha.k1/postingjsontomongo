from flask import Blueprint, request
from scripts.handler.json_mongo_handler import datainserting

mongo_json = Blueprint("json_in_mongo_blueprint", __name__)


@mongo_json.route('/', methods=['POST'])
def postJsonHandler():
    content = request.get_json()
    datainserting(content)
    return str(content)
