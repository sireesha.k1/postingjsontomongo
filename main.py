from flask import Flask
from scripts.service.json_mongo_service import mongo_json

app = Flask(__name__)

app.register_blueprint(mongo_json)
if __name__ == "__main__":
    app.run()
